package main

import (
	"aoc2022/utils"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

const (
	Up    string = "U"
	Down         = "D"
	Left         = "L"
	Right        = "R"
)

type Point struct {
	x int
	y int
}

func (p *Point) toString() string {
	return strconv.Itoa(p.x) + "," + strconv.Itoa(p.y)
}

type State struct {
	h           Point
	t           Point
	tailVisited map[string]int
}

func (s *State) diff() int {
	return int(math.Max(math.Abs(float64(s.h.x-s.t.x)), math.Abs(float64(s.h.y-s.t.y))))
}

func (s *State) visualize() {
	maxX := 0
	maxY := 0

	for point, _ := range s.tailVisited {
		split := strings.Split(point, ",")
		x, _ := strconv.Atoi(split[0])
		y, _ := strconv.Atoi(split[1])
		if x > maxX {
			maxX = x
		}
		if y > maxY {
			maxY = y
		}
	}

	maxX += 1
	//maxY += 1

	for j := maxY; j >= 0; j-- {
		for i := 0; i <= maxX; i++ {
			key := strconv.Itoa(i) + "," + strconv.Itoa(j)
			_, contains := s.tailVisited[key]
			if contains {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Print("\n")
	}
}

func (s *State) move(direction string, count int) {
	for i := 0; i < count; i++ {
		hOld := s.h

		switch direction {
		case Up:
			s.h.y += 1
		case Down:
			s.h.y -= 1
		case Left:
			s.h.x -= 1
		case Right:
			s.h.x += 1
		}

		if s.diff() > 1 {
			s.t = hOld
		}

		_, inMap := s.tailVisited[s.t.toString()]
		if !inMap {
			s.tailVisited[s.t.toString()] += 1
		}
	}
}

func parseLine(line string) (string, int) {
	split := strings.Split(line, " ")
	num, _ := strconv.Atoi(split[1])
	return split[0], num
}

func part1(input []string) {
	result := 0
	state := State{tailVisited: make(map[string]int)}

	for _, line := range input {
		direction, count := parseLine(line)
		state.move(direction, count)
	}

	result = len(state.tailVisited)
	fmt.Printf("part 1 result: %d --- ", result)
}

func part2(input []string) {
	result := 0
	fmt.Printf("part 1 result: %d --- ", result)

}

func main() {
	inputData, err := utils.ReadFile("day09/test_input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	//part2(inputData)
	//fmt.Println(time.Since(start))
}
