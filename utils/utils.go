package utils

import (
	"bufio"
	"os"
	"strings"
)

func ReadFile(filePath string, trimSpace bool) (data []string, err error) {
	pwd, _ := os.Getwd()

	file, err := os.Open(pwd + "/" + filePath)
	if err != nil {
		return nil, err
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		if trimSpace {
			line = strings.TrimSpace(line)
		}
		data = append(data, line)
	}

	return data, nil
}
