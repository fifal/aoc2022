package main

import (
	"aoc2022/utils"
	"fmt"
	"strconv"
	"strings"
	"time"
)

const (
	LS = "ls"
	CD = "cd"
)

type File struct {
	name string
	size int
}

type Dir struct {
	name   string
	parent *Dir
	files  []*File
	dirs   []*Dir
}

type Cmd struct {
	name     string
	argument string
}

var root = Dir{name: "/", parent: nil}

func (d Dir) getTotalSize() int {
	size := 0

	for _, file := range d.files {
		size += file.size
	}
	for _, dir := range d.dirs {
		size += dir.getTotalSize()
	}

	return size
}

func (d Dir) getSolutions(part1Size *int, part2Size *int, missingSize int) int {
	size := 0

	for _, file := range d.files {
		size += file.size
	}
	for _, dir := range d.dirs {
		s := dir.getSolutions(part1Size, part2Size, missingSize)
		size += s

		if s < 100000 {
			*part1Size += s
		}
		if s >= missingSize && s < *part2Size {
			*part2Size = s
		}
	}

	return size
}

func parseCommand(command string) Cmd {
	parts := strings.Split(command, " ")
	argument := ""

	if len(parts) > 2 {
		argument = parts[2]
	}
	return Cmd{name: parts[1], argument: argument}
}

func processCommand(command Cmd, currentDir *Dir, input []string, inputIndex int) *Dir {
	switch command.name {
	case CD:
		if command.argument == "/" {
			return &root
		} else if command.argument == ".." {
			return currentDir.parent
		} else {
			for _, dir := range currentDir.dirs {
				if dir.name == command.argument {
					return dir
				}
			}
		}
	case LS:
		var outputs []string
		slice := input[inputIndex+1:]

		for i, line := range slice {
			if line[0] != '$' && i != len(slice)-1 {
				continue
			}

			outputs = input[inputIndex+1 : inputIndex+1+i+1]
			break
		}

		for _, output := range outputs {
			parts := strings.Split(output, " ")
			if parts[0] == "dir" {
				currentDir.dirs = append(currentDir.dirs, &Dir{name: parts[1], parent: currentDir})
			} else {
				size, _ := strconv.Atoi(parts[0])
				currentDir.files = append(currentDir.files, &File{name: parts[1], size: size})
			}
		}

		return currentDir
	}

	return currentDir
}

func makeTree(input []string) {
	currentDir := &root

	for i, line := range input {
		if line[0] != '$' {
			continue
		}

		cmd := parseCommand(line)
		currentDir = processCommand(cmd, currentDir, input, i)
	}
}

func compute() {
	missing := root.getTotalSize() - (70_000_000 - 30_000_000)
	part1Size, part2Size := 0, 70_000_000

	root.getSolutions(&part1Size, &part2Size, missing)

	fmt.Printf("part 1 result: %d\npart 2 result: %d --- ", part1Size, part2Size)
}

func main() {
	inputData, err := utils.ReadFile("day07/input.txt", false)
	if err != nil {
		panic(err)
	}

	makeTree(inputData)

	start := time.Now()
	compute()
	fmt.Println(time.Since(start))
}
