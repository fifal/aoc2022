package main

import (
	"aoc2022/utils"
	"fmt"
	"strconv"
	"time"
)

func makeArray(input []string) [][]int {
	result := make([][]int, len(input[0]))
	for i, line := range input {
		result[i] = make([]int, len(line))

		for j, tree := range line {
			val, _ := strconv.Atoi(string(tree))
			result[i][j] = val
		}
	}

	return result
}

func max(input []int) int {
	result := 0
	for _, item := range input {
		if item > result {
			result = item
		}
	}
	return result
}

func findVisible(t [][]int) int {
	// Count border trees
	visible := (len(t) * 2) + ((len(t[0]) - 2) * 2)

	for i := 1; i < len(t)-1; i++ {
		for j := 1; j < len(t[i])-1; j++ {
			// Find the highest tree in each direction
			top := make([]int, 0)
			for ti := i - 1; ti >= 0; ti-- {
				top = append(top, t[ti][j])
			}
			bottom := make([]int, 0)
			for bi := i + 1; bi < len(t); bi++ {
				bottom = append(bottom, t[bi][j])
			}
			left := t[i][0:j]
			right := t[i][j+1:]

			c := t[i][j]

			if max(top) < c || max(bottom) < c || max(left) < c || max(right) < c {
				visible += 1
			}

		}
	}

	return visible
}

func findHighestScenicScore(t [][]int) int {
	// Count border trees
	highestScenicScore := 0

	for i := 0; i < len(t); i++ {
		for j := 0; j < len(t[i]); j++ {
			scenicScore := make([]int, 0)
			c := t[i][j]

			index := 1
			for ti := i - 1; ti >= 0; ti-- {
				if t[ti][j] >= c || ti == 0 {
					scenicScore = append(scenicScore, index)
					break
				}
				index++
			}

			index = 1
			for bi := i + 1; bi < len(t); bi++ {
				if t[bi][j] >= c || bi == len(t)-1 {
					scenicScore = append(scenicScore, index)
					break
				}
				index++
			}

			index = 1
			for li := j - 1; li >= 0; li-- {
				if t[i][li] >= c || li == 0 {
					scenicScore = append(scenicScore, index)
					break
				}
				index++
			}

			index = 1
			for ri := j + 1; ri < len(t[i]); ri++ {
				if t[i][ri] >= c || ri == len(t[i])-1 {
					scenicScore = append(scenicScore, index)
					break
				}
				index++
			}

			currentScenicScore := 1
			for _, item := range scenicScore {
				currentScenicScore *= item
			}

			if currentScenicScore > highestScenicScore {
				highestScenicScore = currentScenicScore
			}

		}
	}

	return highestScenicScore
}

func part1(input []string) {
	result := 0
	array := makeArray(input)
	result = findVisible(array)

	fmt.Printf("part 1 result: %d --- ", result)
}

func part2(input []string) {
	result := 0
	array := makeArray(input)
	result = findHighestScenicScore(array)

	fmt.Printf("part 2 result: %d --- ", result)
}

func main() {
	inputData, err := utils.ReadFile("day08/input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	part2(inputData)
	fmt.Println(time.Since(start))
}
