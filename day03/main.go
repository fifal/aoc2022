package main

import (
	"aoc2022/utils"
	"fmt"
	"strings"
)

func findCommon(rucksack string) rune {
	half := len(rucksack) / 2
	firstComp := rucksack[:half]
	secondComp := rucksack[half:]

	for _, r1 := range firstComp {
		if strings.Contains(secondComp, string(r1)) {
			return r1
		}
	}

	fmt.Println(firstComp, secondComp)
	panic("error")
}

func part1(input []string, priority map[rune]int) {
	result := 0
	for _, rucksack := range input {
		common := findCommon(rucksack)
		result += priority[common]
	}

	fmt.Println("part 1 result: ", result)
}

func findBadge(groupInput []string) rune {
	group := make(map[rune]int)

	for i := 0; i < 3; i++ {
		countedChars := make(map[rune]bool)

		for _, ch := range groupInput[i] {
			if countedChars[ch] {
				continue
			}
			group[ch] += 1
			countedChars[ch] = true
		}
	}

	for item, count := range group {
		if count == 3 {
			return item
		}
	}

	panic("shouldn't happen")
}

func part2(input []string, priority map[rune]int) {
	result := 0

	for i := 0; i < len(input); i += 6 {
		firstBadge := findBadge(input[i : i+3])
		secondBadge := findBadge(input[i+3 : i+6])

		result += priority[firstBadge] + priority[secondBadge]
	}

	fmt.Println("part 2 result: ", result)
}

func main() {
	inputData, err := utils.ReadFile("day03/input.txt", false)
	if err != nil {
		panic(err)
	}

	var priorityMap = make(map[rune]int)
	for i := 'a'; i <= 'z'; i++ {
		priorityMap[i] = int(i-'a') + 1
	}
	for i := 'A'; i <= 'Z'; i++ {
		priorityMap[i] = int(i-'A') + 27
	}
	fmt.Println(priorityMap)

	part1(inputData, priorityMap)
	part2(inputData, priorityMap)

}
