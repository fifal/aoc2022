package main

import (
	"aoc2022/utils"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	ADD      = "+"
	MULTIPLY = "*"
)

type Operation struct {
	op    string
	value int
}

type Monkey struct {
	items           []int
	operation       Operation
	testValue       int
	trueTest        int
	falseTest       int
	inspectionCount int
}

type Game struct {
	currentMonkey int
	monkeys       []*Monkey
}

func (g *Game) playRound(mod int) {
	for _, monkey := range g.monkeys {
		for _, item := range monkey.items {
			opValue := item
			worryLevel := item

			if mod != -1 {
				opValue = item % mod
				worryLevel = item % mod
			}

			if monkey.operation.value != -1 {
				opValue = monkey.operation.value
			}

			if monkey.operation.op == ADD {
				worryLevel += opValue
			} else {
				worryLevel *= opValue
			}

			if mod == -1 {
				worryLevel /= 3
			}

			monkeyIndex := monkey.trueTest
			if worryLevel%monkey.testValue != 0 {
				monkeyIndex = monkey.falseTest
			}

			// Throw
			g.monkeys[monkeyIndex].items = append(g.monkeys[monkeyIndex].items, worryLevel)
			monkey.inspectionCount++
		}

		monkey.items = monkey.items[:0]
	}
}

func (g *Game) simulate(rounds int, decreaseWorryLevel bool) {
	mod := 1
	if !decreaseWorryLevel {
		for _, monkey := range g.monkeys {
			mod *= monkey.testValue
		}
	} else {
		mod = -1
	}

	for i := 0; i < rounds; i++ {
		g.playRound(mod)
	}
}

func (g *Game) getMonkeyBusiness(n int) int {
	result := 1
	values := make([]int, 0)

	for _, monkey := range g.monkeys {
		values = append(values, monkey.inspectionCount)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(values)))
	values = values[:n]

	for _, value := range values {
		result *= value
	}

	return result
}

func getLineValue(line string) int {
	line = strings.TrimSpace(line)
	split := strings.Split(line, " ")
	val, _ := strconv.Atoi(split[len(split)-1])
	return val
}

func parseMonkey(lines []string) Monkey {
	if len(lines) != 6 {
		panic("...")
	}

	lineIndex := 1

	// Parse items
	items := make([]int, 0)
	itemsSplit := strings.Split(strings.TrimSpace(lines[lineIndex][strings.Index(lines[lineIndex], ":")+1:]), ",")
	for _, item := range itemsSplit {
		val, _ := strconv.Atoi(strings.TrimSpace(item))
		items = append(items, val)
	}
	lineIndex++

	// Parse operation
	op := ""
	opLine := lines[lineIndex]
	if strings.Contains(opLine, "*") {
		op = "*"
	} else {
		op = "+"
	}
	opValString := strings.TrimSpace(opLine[strings.Index(opLine, op)+1:])
	opVal := -1
	if opValString != "old" {
		val, _ := strconv.Atoi(opValString)
		opVal = val
	}
	lineIndex++

	// Parse test
	testVal := getLineValue(lines[lineIndex])
	lineIndex++

	trueTest := getLineValue(lines[lineIndex])
	lineIndex++

	falseTest := getLineValue(lines[lineIndex])

	return Monkey{
		items:     items,
		operation: Operation{op: op, value: opVal},
		testValue: testVal,
		trueTest:  trueTest,
		falseTest: falseTest,
	}
}

func part1(input []string) {
	game := Game{}

	for i := 0; i < len(input); i += 7 {
		monkey := parseMonkey(input[i : i+6])
		game.monkeys = append(game.monkeys, &monkey)
	}

	game.simulate(20, true)

	fmt.Printf("part 1 result: %d --- ", game.getMonkeyBusiness(2))
}

func part2(input []string) {
	game := Game{}

	for i := 0; i < len(input); i += 7 {
		monkey := parseMonkey(input[i : i+6])
		game.monkeys = append(game.monkeys, &monkey)
	}

	game.simulate(10000, false)
	fmt.Printf("part 2 result: %d --- ", game.getMonkeyBusiness(2))
}

func main() {
	inputData, err := utils.ReadFile("day11/input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	part2(inputData)
	fmt.Println(time.Since(start))
}
