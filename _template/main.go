package main

import (
	"aoc2022/utils"
	"fmt"
	"time"
)

func part1(input []string) {
	result := 0
	fmt.Printf("part 1 result: %d --- ", result)
}

func part2(input []string) {
	result := 0
	fmt.Printf("part 2 result: %d --- ", result)
}

func main() {
	inputData, err := utils.ReadFile("dayxx/input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	part2(inputData)
	fmt.Println(time.Since(start))
}
