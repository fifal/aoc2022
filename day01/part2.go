package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	pwd, _ := os.Getwd()

	data, err := os.Open(pwd + "/day01/input.txt")
	if err != nil {
		panic(err)
	}

	fileScanner := bufio.NewScanner(data)
	fileScanner.Split(bufio.ScanLines)

	index := 0
	sum := 0
	var m []int

	for fileScanner.Scan() {
		calNum, err := strconv.Atoi(fileScanner.Text())

		if err != nil {
			m = append(m, sum)
			sum = 0
			index += 1
			continue
		}

		sum += calNum
	}

	sort.Sort(sort.Reverse(sort.IntSlice(m)))
	result := m[0:3]
	totalSum := 0
	for _, val := range result {
		totalSum += val
	}
	fmt.Println(totalSum)
}
