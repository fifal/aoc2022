package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	pwd, _ := os.Getwd()

	data, err := os.Open(pwd + "/day01/input.txt")
	if err != nil {
		panic(err)
	}

	fileScanner := bufio.NewScanner(data)
	fileScanner.Split(bufio.ScanLines)

	index := 0
	sum := 0
	maxSum := 0
	maxIndex := 0

	for fileScanner.Scan() {
		calNum, err := strconv.Atoi(fileScanner.Text())

		if err != nil {
			if sum > maxSum {
				maxSum = sum
				maxIndex = index
			}

			sum = 0
			index += 1
			continue
		}

		sum += calNum
	}

	fmt.Println(maxIndex, maxSum)
}
