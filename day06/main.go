package main

import (
	"aoc2022/utils"
	"fmt"
	"strings"
	"time"
)

func containsUnique(part string) bool {
	alreadyFound := ""
	for _, char := range part {
		if strings.Contains(alreadyFound, string(char)) {
			return false
		}
		alreadyFound += string(char)
	}
	return true
}

func findUniquePart(input string, n int) int {
	for i := 0; i < len(input)-n; i++ {
		part := input[i : i+n]
		if containsUnique(part) {
			return strings.Index(input, part) + n
		}
	}
	return -1
}

func part1(input []string) {
	result := findUniquePart(input[0], 4)

	fmt.Printf("part 1 result: %d --- ", result)
}

func part2(input []string) {
	result := findUniquePart(input[0], 14)

	fmt.Printf("part 2 result: %d --- ", result)

}

func main() {
	inputData, err := utils.ReadFile("day06/input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	part2(inputData)
	fmt.Println(time.Since(start))

}
