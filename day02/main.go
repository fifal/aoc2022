package main

import (
	"aoc2022/utils"
	"fmt"
	"strings"
)

const TIE = 3
const WIN = 6

// A,X: Rock, B,Y: Paper, C,Z: Scissors
var valueMap = map[int]int{'X': 1, 'Y': 2, 'Z': 3}
var winMap = map[int]int{'A': 'Y', 'B': 'Z', 'C': 'X'}
var tieMap = map[int]int{'A': 'X', 'B': 'Y', 'C': 'Z'}
var loseMap = map[int]int{'A': 'Z', 'B': 'X', 'C': 'Y'}

func roundResult(elf int, our int) int {
	// We won
	if our == winMap[elf] {
		return valueMap[our] + WIN
	} else if our == tieMap[elf] {
		return valueMap[our] + TIE
	} else {
		return valueMap[our]
	}
}

func roundResult2(elf int, outcome int) int {
	// Lose
	if string(rune(outcome)) == "X" {
		return valueMap[loseMap[elf]]
	} else if string(rune(outcome)) == "Y" {
		return valueMap[tieMap[elf]] + TIE
	} else {
		return valueMap[winMap[elf]] + WIN
	}
}

func part1(input []string) {
	result := 0
	for _, line := range input {
		split := strings.Split(line, " ")
		elf := int(split[0][0])
		our := int(split[1][0])
		result += roundResult(elf, our)
	}

	fmt.Println("part 1 result: ", result)
}

func part2(input []string) {
	result := 0
	for _, line := range input {
		split := strings.Split(line, " ")
		elf := int(split[0][0])
		our := int(split[1][0])
		result += roundResult2(elf, our)
	}

	fmt.Println("part 2 result: ", result)
}

func main() {
	inputData, err := utils.ReadFile("day02/input.txt", false)
	if err != nil {
		panic(err)
	}

	part1(inputData)
	part2(inputData)
}
