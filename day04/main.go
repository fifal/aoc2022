package main

import (
	"aoc2022/utils"
	"fmt"
	"strconv"
	"strings"
	"time"
)

func splitPairs(input string) ([]int, []int) {
	split := strings.Split(input, ",")
	f := strings.Split(split[0], "-")
	s := strings.Split(split[1], "-")

	fLow, _ := strconv.Atoi(f[0])
	fUp, _ := strconv.Atoi(f[1])
	sLow, _ := strconv.Atoi(s[0])
	sUp, _ := strconv.Atoi(s[1])

	var first = make([]int, 0)
	var second = make([]int, 0)

	for j := fLow; j <= fUp; j++ {
		first = append(first, j)
	}
	for j := sLow; j <= sUp; j++ {
		second = append(second, j)
	}

	if len(first) > len(second) {
		return first, second
	} else {
		return second, first
	}
}

func arrayContains(bigger *[]int, smaller *[]int) bool {
	for _, item1 := range *smaller {
		found := false
		for _, item2 := range *bigger {
			if item2 == item1 {
				found = true
				break
			}
		}

		if !found {
			return false
		}
	}

	return true
}

func arrayIncludesAny(bigger *[]int, smaller *[]int) bool {
	for _, item1 := range *smaller {
		for _, item2 := range *bigger {
			if item2 == item1 {
				return true
			}
		}
	}
	return false
}

func part1(input []string) {
	var result int = 0

	for i := 0; i < len(input); i++ {
		bigger, smaller := splitPairs(input[i])

		if arrayContains(&bigger, &smaller) {
			result += 1
		}
	}

	fmt.Printf("part 1 result: %d --- ", result)
}

func part2(input []string) {
	result := 0

	for i := 0; i < len(input); i++ {
		bigger, smaller := splitPairs(input[i])

		if arrayIncludesAny(&bigger, &smaller) {
			result += 1
		}
	}

	fmt.Printf("part 2 result: %d --- ", result)
}

func main() {
	inputData, err := utils.ReadFile("day04/input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	part2(inputData)
	fmt.Println(time.Since(start))
}
