package main

import (
	"aoc2022/utils"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func createBoxStacks(input []string) (map[int][]string, int) {
	stacks := make(map[int][]string)

	for index, line := range input {
		if strings.Contains(line, "2") {
			return stacks, index + 2
		}

		stackNo := 1
		for i := 0; i < len(line)-1; i += 4 {
			box := line[i+1 : i+2]
			box = strings.TrimSpace(box)
			if box == "" {
				stackNo++
				continue
			}
			stacks[stackNo] = append(stacks[stackNo], box)
			stackNo++
		}
	}

	return nil, -1
}

func moveBoxes(n int, from int, to int, stacks map[int][]string) {
	for i := 0; i < n; i++ {
		box := stacks[from][0]
		stacks[from] = stacks[from][1:]
		stacks[to] = append([]string{box}, stacks[to]...)
	}
}

func moveMultipleBoxes(n int, from int, to int, stacks map[int][]string) {
	boxes := stacks[from][0:n]
	stacks[from] = stacks[from][n:]
	temp := make([]string, 0)
	temp = append(temp, boxes...)
	stacks[to] = append(temp, stacks[to]...)
}

func parseInstruction(instruction string) (int, int, int) {
	re := regexp.MustCompile(`move (?P<n>\d+) from (?P<from>\d+) to (?P<to>\d+)`)
	result := re.FindStringSubmatch(instruction)

	n, _ := strconv.Atoi(result[re.SubexpIndex("n")])
	from, _ := strconv.Atoi(result[re.SubexpIndex("from")])
	to, _ := strconv.Atoi(result[re.SubexpIndex("to")])

	return n, from, to
}

func part1(input []string) {
	result := ""

	stacks, insStartIdx := createBoxStacks(input)

	for i := insStartIdx; i < len(input); i++ {
		instruction := input[i]
		if instruction == "" {
			continue
		}

		n, from, to := parseInstruction(instruction)
		moveBoxes(n, from, to, stacks)
	}

	for i := 1; i <= len(stacks); i++ {
		result += stacks[i][0]
	}

	fmt.Printf("part 1 result: %s --- ", result)
}

func part2(input []string) {
	result := ""

	stacks, insStartIdx := createBoxStacks(input)

	for i := insStartIdx; i < len(input); i++ {
		instruction := input[i]
		if instruction == "" {
			continue
		}

		n, from, to := parseInstruction(instruction)
		moveMultipleBoxes(n, from, to, stacks)
	}

	for i := 1; i <= len(stacks); i++ {
		result += stacks[i][0]
	}

	fmt.Printf("part 2 result: %s --- ", result)

}

func main() {
	inputData, err := utils.ReadFile("day05/input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	part2(inputData)
	fmt.Println(time.Since(start))

}
