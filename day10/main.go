package main

import (
	"aoc2022/utils"
	"fmt"
	"strconv"
	"strings"
	"time"
)

const (
	Addx = "addx"
	Noop = "noop"
)

var cycles = map[int]bool{20: true, 60: true, 100: true, 140: true, 180: true, 220: true}

func parseLine(line string) (string, int) {
	split := strings.Split(line, " ")
	if len(split) == 1 {
		return split[0], -1
	}

	num, _ := strconv.Atoi(split[1])
	return split[0], num
}

func part1(input []string) {
	results := make([]int, 0)

	regVal := 1
	cycle := 1
	sleep := 0
	i := 0
	for cycle <= 220 {
		instr, num := parseLine(input[i])

		if cycles[cycle] {
			results = append(results, cycle*regVal)
		}

		if sleep > 0 {
			sleep--
			cycle++
			if sleep == 0 {
				regVal += num
				i++
				continue
			}
			continue
		}

		if instr == Noop {
			cycle++
		}
		if instr == Addx {
			cycle++
			sleep = 1
			continue
		}

		i++
	}

	result := 0
	for _, item := range results {
		result += item
	}

	fmt.Printf("part 1 result: %d --- ", result)
}

func part2(input []string) {
	regVal := 1
	cycle := 1
	sleep := 0
	i := 0
	row := 0

	rows := make([][]string, 6)
	for r, _ := range rows {
		rows[r] = make([]string, 40)
	}

	for cycle <= 240 {
		instr, num := parseLine(input[i])

		drCycle := cycle - 1
		if drCycle%40 == 0 && drCycle != 0 {
			row += 1
		}

		if regVal-1 <= drCycle%40 && regVal+1 >= drCycle%40 {
			rows[row][drCycle%40] = "#"
		} else {
			rows[row][drCycle%40] = "."
		}

		if sleep > 0 {
			sleep--
			cycle++
			if sleep == 0 {
				regVal += num
				i++
				continue
			}
			continue
		}

		if instr == Noop {
			cycle++
		}
		if instr == Addx {
			cycle++
			sleep = 1
			continue
		}

		i++
	}

	fmt.Printf("part 2 result: \n")
	for _, item := range rows {
		fmt.Println(item)
	}

}

func main() {
	inputData, err := utils.ReadFile("day10/input.txt", false)
	if err != nil {
		panic(err)
	}

	start := time.Now()
	part1(inputData)
	fmt.Println(time.Since(start))

	part2(inputData)
	fmt.Println(time.Since(start))
}
